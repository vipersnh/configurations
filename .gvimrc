set guifont=PT\ Mono:h14
set hlsearch
syn keyword Todo TODO
set syntax=vim
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
set tags=./tags,tags;$HOME
set nu
set ruler
